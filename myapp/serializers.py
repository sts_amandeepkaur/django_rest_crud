from rest_framework import serializers
from myapp import models

class DesignSerializer(serializers.ModelSerializer):
    pageId = serializers.IntegerField()
    name = serializers.CharField(max_length=250)
    class Meta:
        model = models.Design
        fields = "__all__"

class configurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.configuration
        fields="__all__"
