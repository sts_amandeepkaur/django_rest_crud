from django.db import models

class configuration(models.Model):
    icon = models.CharField(max_length=250,blank=True)
    style = models.CharField(max_length=250,blank=True)
    position = models.CharField(max_length=250,blank=True)
    styleCSS = models.TextField(blank=True)
    buttonSize = models.CharField(max_length=250,blank=True)
    buttonText = models.CharField(max_length=250,blank=True)
    headerText = models.CharField(max_length=250,blank=True)
    buttonColor = models.CharField(max_length=250,blank=True)
    optlnMessage = models.CharField(max_length=250,blank=True)
    backgroundType = models.CharField(max_length=250,blank=True)
    backgroundColor = models.CharField(max_length=250,blank=True)
    backgroundImage = models.FileField(upload_to="media/%Y/%m/%d",blank=True)
    backgroundVideo = models.FileField(upload_to="media/%Y/%m/%d",blank=True)
    descriptionText = models.CharField(max_length=250,blank=True)
    headerTextColor = models.CharField(max_length=250,blank=True)
    descriptionTextColor = models.CharField(max_length=250,blank=True)
    
    def __str__(self):
        return str(self.id)

class Design(models.Model):
    pageId= models.IntegerField()
    name = models.CharField(max_length=250)
    type = models.CharField(max_length=250)
    uuid = models.CharField(max_length=250)
    configuration = models.ForeignKey(configuration,on_delete=models.CASCADE, related_name='designs')
    description = models.TextField(blank=True)
    created_on= models.DateTimeField(auto_now_add=True)
    updated_on= models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
    

