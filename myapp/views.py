from rest_framework import viewsets
from myapp import models
from myapp import serializers
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

class DesignViewset(viewsets.ModelViewSet):
    queryset = models.Design.objects.all()
    serializer_class = serializers.DesignSerializer

    #All
    def list(self, request):
        queryset = models.Design.objects.all()
        serializer = serializers.DesignSerializer(queryset, many=True)
        return Response(serializer.data)
    #Retrieve
    def retrieve(self, request, pk=None):
        queryset = models.Design.objects.all()
        design = get_object_or_404(queryset, pk=pk)
        serializer = serializers.DesignSerializer(design)
        return Response(serializer.data)

    #Update Record
    def update(self, request, pk=None):
        queryset = models.Design.objects.all()
        design = get_object_or_404(queryset, pk=pk)
        serializer = serializers.DesignSerializer(design,data=request.data)
        if serializer.is_valid():
            serializer.save()
        return Response(serializer.data)

    ## Delete Record
    def destroy(self, request, pk=None):
        queryset = models.Design.objects.all()
        design = get_object_or_404(queryset, pk=pk)
        serializer = serializers.DesignSerializer(design,data=request.data)
        if serializer.is_valid():
            serializer.remove()
        return Response(serializer.data)
    
class configurationViewset(viewsets.ModelViewSet):
    queryset = models.configuration.objects.all()
    serializer_class = serializers.configurationSerializer