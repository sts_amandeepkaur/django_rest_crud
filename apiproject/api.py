from rest_framework import routers
from myapp import views

router = routers.DefaultRouter()
router.register('designs', views.DesignViewset,basename="designs")
router.register('configurations', views.configurationViewset)

urlpatterns = router.urls
